package com.neuedu.his.dao;

import com.neuedu.his.entity.Register;

import java.util.List;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-16   14:04
 * 作者 :jshand
 * 描述 :
 */
public interface One2OneMapper {

    //默认自动的把所有数据都查出来了
    public List<Register> selectOne2One();


    //优先查询主表的信息,当需要的时候在查从表
    public List<Register> selectOne2One2();

}
