package com.neuedu.his.dao;

import com.neuedu.his.entity.Register;
import com.neuedu.his.entity.Role;
import com.neuedu.his.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSessionFactory;

import java.io.IOException;
import java.util.List;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-16   14:04
 * 作者 :jshand
 * 描述 :
 */
public interface One2ManyMapper {


    /**
     * 一对多的查询
     * @return
     */
    public List<Role> selectOne2Many();


    /**
     * 延迟加载
     * @return
     */
    public List<Role> selectOne2Many2();

}
