package com.neuedu.his.dao;

import com.neuedu.his.entity.CheckItem;
import com.neuedu.his.entity.CheckItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CheckItemMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    long countByExample(CheckItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    int deleteByExample(CheckItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    int insert(CheckItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    int insertSelective(CheckItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    List<CheckItem> selectByExample(CheckItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    CheckItem selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    int updateByExampleSelective(@Param("record") CheckItem record, @Param("example") CheckItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    int updateByExample(@Param("record") CheckItem record, @Param("example") CheckItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    int updateByPrimaryKeySelective(CheckItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table check_item
     *
     * @mbg.generated Wed Mar 16 10:11:12 CST 2022
     */
    int updateByPrimaryKey(CheckItem record);
}