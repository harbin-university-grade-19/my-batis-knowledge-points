package com.neuedu.his.dao;

import com.neuedu.his.entity.Role;
import com.neuedu.his.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-17   10:42
 * 作者 :jshand
 * 描述 :
 */
public class RoleMapperTest {

    SqlSessionFactory factory ;

    @Before
    public void init(){
        try {
            factory = MyBatisUtils.getFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testCache(){

        SqlSession session = factory.openSession();


        RoleMapper mapper = session.getMapper(RoleMapper.class);

        Integer id = 1;
        Role role1 = mapper.selectByPrimaryKey(id);
        System.out.println(role1);


        //更新数据
        role1.setRoleName("无敌管理员");
        mapper.updateByPrimaryKey(role1);


        //第二次查询使用的是缓存中的数据 (如果发生的了DML操作，update、insert、delete)
        Role role2 = mapper.selectByPrimaryKey(id);
        System.out.println(role2);


        System.out.println("role1==role2 = " + (role1 == role2));


        session.close();

    }



}