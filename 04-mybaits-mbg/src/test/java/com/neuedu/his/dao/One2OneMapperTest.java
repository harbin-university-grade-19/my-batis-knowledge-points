package com.neuedu.his.dao;

import com.neuedu.his.entity.Register;
import com.neuedu.his.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-16   14:19
 * 作者 :jshand
 * 描述 :
 */
public class One2OneMapperTest {
    SqlSessionFactory factory ;

    @Before
    public void init(){
        try {
            factory = MyBatisUtils.getFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void selectOne2One() {
        SqlSession session = factory.openSession();
        One2OneMapper mapper = session.getMapper(One2OneMapper.class);

        List<Register> registers = mapper.selectOne2One();
        System.out.println(registers);

        session.close();

    }


    @Test
    public void selectOne2One2() {
        SqlSession session = factory.openSession();
        One2OneMapper mapper = session.getMapper(One2OneMapper.class);

        List<Register> registers = mapper.selectOne2One2();
//        for (Register register : registers) {
//            System.out.println("register.getName() = " + register.getName());
//        }

        for (int i = 0; i < registers.size(); i++) {
            Register register = registers.get(i);
            System.out.println("register.getName() = " + register.getName());
        }

        session.close();

    }
}