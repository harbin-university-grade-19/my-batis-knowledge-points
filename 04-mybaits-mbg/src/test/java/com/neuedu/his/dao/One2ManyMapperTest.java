package com.neuedu.his.dao;

import com.neuedu.his.entity.Register;
import com.neuedu.his.entity.Role;
import com.neuedu.his.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-17   9:23
 * 作者 :jshand
 * 描述 :
 */
public class One2ManyMapperTest {
    SqlSessionFactory factory ;

    @Before
    public void init(){
        try {
            factory = MyBatisUtils.getFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void selectOne2Many() {
        SqlSession session = factory.openSession();
        One2ManyMapper mapper = session.getMapper(One2ManyMapper.class);

        List<Role> roles = mapper.selectOne2Many();

        //roles角色有几个
        System.out.println(roles);


        session.close();
    }


    @Test
    public void selectOne2Many2() {
        SqlSession session = factory.openSession();
        One2ManyMapper mapper = session.getMapper(One2ManyMapper.class);

        List<Role> roles = mapper.selectOne2Many2();

        //roles角色有几个
        System.out.println(roles);


        session.close();
    }
}