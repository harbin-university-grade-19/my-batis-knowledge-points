package com.neuedu.his.dao;

import com.neuedu.his.entity.User;
import com.neuedu.his.entity.UserExample;
import com.neuedu.his.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-16   10:42
 * 作者 :jshand
 * 描述 :
 */
public class UserMapperTest {

    SqlSessionFactory factory ;

    @Before
    public void init(){
        try {
            factory = MyBatisUtils.getFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 根据条件查询
     */
    @Test
    public void selectByExample() {

        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);

        UserExample example = new  UserExample();
        //没有构造条件
        //使用Criteria创建条件片段 ()
        /**
         *  ( username like ? and password = ? )
         */
        UserExample.Criteria c = example.createCriteria();
        c.andUsernameLike("j");
        c.andPasswordEqualTo("123456");

        /**
         * or( password = ? )
         */
        UserExample.Criteria c2  = example.or();
        c2.andPasswordEqualTo("123456");

        List<User> users = mapper.selectByExample(example);

        System.out.println("user = " + users);

        session.close();
    }

    /**
     * 根据主键查询
     */
    @Test
    public void selectByPrimaryKey() {

        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);

        Integer id = 1;
        User user = mapper.selectByPrimaryKey(id);

        System.out.println("user = " + user);

        session.close();
    }

    @Test
    public void countByExample() {
    }

    @Test
    public void deleteByExample() {
        SqlSession session = factory.openSession();
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        //()
        criteria.andUsernameEqualTo("proot");  //( username ='proot')

        try {
            UserMapper mapper = session.getMapper(UserMapper.class);
            int deleteCount = mapper.deleteByExample(example);
            System.out.println("deleteCount = " + deleteCount);
            session.commit();
        }catch (Exception e){
            session.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    @Test
    public void deleteByPrimaryKey() {
        SqlSession session = factory.openSession();
        try {
            UserMapper mapper = session.getMapper(UserMapper.class);
            Integer id = 8;
            int deleteCount = mapper.deleteByPrimaryKey(id);
            System.out.println("deleteCount = " + deleteCount);
            session.commit();
        }catch (Exception e){
            session.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

    }

    @Test
    public void insert() {
        User user = new User();
        user.setUserId(8);
        user.setUsername("jqk");
        user.setPassword("123456");

        //没有设置active，createTime ,会插入空的

        SqlSession session = factory.openSession();
        try {
            UserMapper mapper = session.getMapper(UserMapper.class);

            int count = mapper.insert(user);
            System.out.println("count = " + count);

            session.commit();
        }catch (Exception e){
            session.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

    }

    @Test
    public void insertSelective() {
        User user = new User();
//        user.setUserId(8);
        user.setUsername("jqk");
        user.setPassword("123456");

        //没有设置active，createTime ,会插入空的

        SqlSession session = factory.openSession();
        try {
            UserMapper mapper = session.getMapper(UserMapper.class);

            int count = mapper.insertSelective(user);
            System.out.println("count = " + count);

            session.commit();
        }catch (Exception e){
            session.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }

    }



    @Test
    public void updateByExampleSelective() {
    }

    @Test
    public void updateByExample() {
    }

    @Test
    public void updateByPrimaryKeySelective() {

        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);

        Integer id = 2;
        User user = mapper.selectByPrimaryKey(id);
        user.setRealname("真实姓名2222");
        user.setActive(null); //修改
        user.setCreatetime(null);//修改成null
        try {
            int count = mapper.updateByPrimaryKeySelective(user);
            System.out.println("count = " + count);
            session.commit();
        }catch (Exception e){
            session.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }



    }

    @Test
    public void updateByPrimaryKey() {

        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);

        Integer id = 1;
        User user = mapper.selectByPrimaryKey(id);
        user.setRealname("真实姓名");
        user.setActive(null); //修改
        user.setCreatetime(null);//修改成null
        try {
            int count = mapper.updateByPrimaryKey(user);
            System.out.println("count = " + count);
            session.commit();
        }catch (Exception e){
            session.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }



    }
}