package com.neuedu.mybatis.dao;

import com.neuedu.mybatis.entity.Dept;
import com.neuedu.mybatis.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-15   13:55
 * 作者 :jshand
 * 描述 :
 */
public class DeptMapperTest {

    @Test
    public void selectList() throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();

        SqlSession sqlSession = factory.openSession();
        //从Session中获取一个DeptMapper接口的 代理类 （Mybatis帮我们实现的代理类）;
        
        //DeptMapper deptMapper 接口
        DeptMapper deptMapper  = sqlSession.getMapper(DeptMapper.class);

        System.out.println("deptMapper = " + deptMapper);
        List<Dept> depts = deptMapper.selectList();

       
        depts.forEach(System.out::println);

        sqlSession.close();

    }

    @Test
    public void selectById() {
    }

    @Test
    public void insert() {
    }

    @Test
    public void updateByPrimaryKey() {
    }

    @Test
    public void deleteByPrimaryKey() {
    }

    @Test
    public void selectCount() {
    }

    @Test
    public void selectListByCondition() throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();

        SqlSession sqlSession = factory.openSession();

        //DeptMapper deptMapper 接口
        DeptMapper deptMapper  = sqlSession.getMapper(DeptMapper.class);

        /**
         * SELECT `id`, `name`, `address`, `leader`, `active`, `createtime`,
         *  `is_useful` FROM `department` where 1=1
         */
        Dept dept = new Dept();
        /**
         * SELECT `id`, `name`, `address`, `leader`, `active`, `createtime`,
         * `is_useful` FROM `department` where 1=1 and name like ?
         */
//        dept.setName("%科%");
        dept.setName("科");

        dept.setAddress("%3%");
        List<Dept> depts = deptMapper.selectListByCondition(dept);
        System.out.println("depts = " + depts);

        sqlSession.close();
    }




    @Test
    public void selectListByCondition2() throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();

        SqlSession sqlSession = factory.openSession();

        //DeptMapper deptMapper 接口
        DeptMapper deptMapper  = sqlSession.getMapper(DeptMapper.class);

        Dept dept = new Dept();
//        dept.setName("科");
//        dept.setAddress("%3%");

        List<Dept> depts = deptMapper.selectListByCondition2(dept);
        System.out.println("depts = " + depts);

        sqlSession.close();
    }

    @Test
    public void selectListByBind() throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();

        SqlSession sqlSession = factory.openSession();

        //DeptMapper deptMapper 接口
        DeptMapper deptMapper  = sqlSession.getMapper(DeptMapper.class);

        Dept dept = new Dept();
        dept.setName("科");

        List<Dept> depts = deptMapper.selectListByBind(dept);
        System.out.println("depts = " + depts);

        sqlSession.close();
    }

    @Test
    public void selectByIds() throws IOException {

        SqlSessionFactory factory = MyBatisUtils.getFactory();

        SqlSession sqlSession = factory.openSession();

        //DeptMapper deptMapper 接口
        DeptMapper deptMapper  = sqlSession.getMapper(DeptMapper.class);

//        List ids =  Arrays.asList(1,2,3,4,5);
//        List ids =  Arrays.asList(1,3);
//        List<Dept> depts = deptMapper.selectByIds(ids);


        Integer[] ids = new Integer[]{1,3};
        List<Dept> depts = deptMapper.selectByIds2(ids);
        depts.forEach(System.out::println);

        sqlSession.close();

    }
}