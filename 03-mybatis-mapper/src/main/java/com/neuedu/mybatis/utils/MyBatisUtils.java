package com.neuedu.mybatis.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * 项目：com.neuedu.mybatis
 * 创建时间：  2022-03-14   13:46
 * 作者 :jshand
 * 描述 :
 */
public class MyBatisUtils {

    //重量级，线程安全的，全局唯一， 单例模式
    private static SqlSessionFactory sqlSessionFactory;
    //默认的配置文件
    private static final String path = "SqlMapConfig.xml";

    public static synchronized SqlSessionFactory getFactory(String resource) throws IOException {

        if (sqlSessionFactory == null) {
            //Resources 到MyBatis的包
            InputStream is = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        }
        return sqlSessionFactory;
    }

    public static synchronized SqlSessionFactory getFactory() throws IOException {
        return getFactory(path);

    }

}
