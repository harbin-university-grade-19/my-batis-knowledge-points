package com.neuedu.mybatis.dao;

import com.neuedu.mybatis.entity.Dept;
import org.apache.ibatis.annotations.Param;


import java.io.IOException;
import java.util.List;

/**
 * 项目：com.neuedu.mybatis
 * 创建时间：  2022-03-15   13:41
 * 作者 :jshand
 * 描述 :  com.neuedu.mybatis.dao.DeptMapper
 */
public interface DeptMapper {

    public List<Dept> selectList() throws IOException ;

    public Dept selectById(Integer id) throws IOException;

    public int insert(Dept dept ) throws IOException;

    public int updateByPrimaryKey( Dept dept) throws IOException ;

    public int deleteByPrimaryKey( Integer id) throws IOException ;

    /**
     * 查询总条数
     */

    public int  selectCount() throws IOException ;


    public List<Dept>  selectListByCondition(Dept dept);
    public List<Dept>  selectListByCondition2(Dept dept);
    public List<Dept>  selectListByBind(Dept dept);

    public List<Dept>  selectByIds(@Param("ids") List ids);

    public List<Dept>  selectByIds2(Integer[] ids);

}
