package com.neuedu.mybatis;

import com.neuedu.mybaits.entity.Dept;
import com.neuedu.mybaits.utils.MyBatisUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-14   9:37
 * 作者 :jshand
 * 描述 :
 */
public class MyBatis2Test {
//    SqlSessionFactory sqlSessionFactory  = null;
//    @Before
//    public void methodInit() throws IOException {
//        String resource = "SqlMapConfig.xml";
//        //Resources 到MyBatis的包
//        InputStream is = Resources.getResourceAsStream(resource);
////        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
//        sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
//    }



    @Test
    public void test1() throws IOException {


        //打开会话
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        SqlSession sqlSession = factory.openSession();

        //执行sql语句
        String statement = "helloworld.selectDept1";//指的是sql语句（MapperStatement）的ID
        List<Map> objects = sqlSession.selectList(statement);

        for (Map row : objects) {
//            System.out.println("object.getClass() = " + row.getClass());
//            System.out.println("object = " + row);//每一行的结果
            Integer id = (Integer) row.get("id");
            String name = (String) row.get("name1");  //name1是不对的，但是程序编译通过 语法对的
            System.out.printf("id:%d,name:%s\r\n",id,name);
        }


        sqlSession.close();

    }




    @Test
    public void test2() throws IOException {

        //打开会话
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        SqlSession sqlSession = factory.openSession();

        //执行sql语句
        String statement = "helloworld.selectDept2";//指的是sql语句（MapperStatement）的ID
        List<Dept> deptList = sqlSession.selectList(statement);

        for (Dept dept : deptList) {
//            dept.getId1() //强制检查的，在编阶段就能发现问题
            System.out.println(dept);
        }

        sqlSession.close();

    }

}
