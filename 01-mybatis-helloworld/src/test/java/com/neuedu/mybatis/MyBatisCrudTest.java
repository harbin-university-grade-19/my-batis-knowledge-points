package com.neuedu.mybatis;

import com.neuedu.mybaits.entity.Dept;
import com.neuedu.mybaits.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-14   14:06
 * 作者 :jshand
 * 描述 :
 */
public class MyBatisCrudTest {

    @Test
    public void testSelectById() throws IOException {

        SqlSessionFactory factory = MyBatisUtils.getFactory();
        //设计事务默认不自动提交
        SqlSession session = factory.openSession();

        Integer id = 1;
        Dept dept = session.selectOne("helloworld.selectById", id);

        System.out.println(dept);

        session.close();
    }


    @Test
    public void testInsert() throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        //设计事务默认不自动提交
        SqlSession session = factory.openSession();

        String statementId = "helloworld.insert";

        // parameterType="com.neuedu.mybaits.entity.Dept"
        Dept dept = new Dept();
        dept.setName("保安室");
        dept.setAddress("保安大队001");
        dept.setActive(1);
        dept.setCreateTime(new Date());//创建数据的时间，当前时间
        dept.setLeader("陈佩斯");
        dept.setUseFul("可用");

        int count = session.insert(statementId, dept);
        System.out.println("count = " + count);

        //session提交事务
        session.commit();

        //关闭资源
        session.close();
    }

    @Test
    public void testUpdate() throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        //设计事务默认不自动提交
        SqlSession session = factory.openSession();

        try {
            //查询一条数据，
            Integer id = 1;
            Dept dept = session.selectOne("helloworld.selectById", id);

            //改数据（setXXX）
            dept.setName("管理医生的科室");
            dept.setAddress("2301");

            //更新操作
            int count = session.update("helloworld.updateByPrimaryKey", dept);
            //提交事务
            session.commit();
        } catch (Exception e) {
            //回滚
            session.rollback();
            e.printStackTrace();
        } finally {
            //关闭资源
            session.close();
        }
    }

    @Test
    public void testDeleteByPrimaryKey() throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        //设计事务默认不自动提交
        SqlSession session = factory.openSession();

        try {
            //根据主键删除
            Integer id = 4;
            int deleteCount = session.delete("helloworld.deleteByPrimaryKey", id);
            System.out.println("deleteCount = " + deleteCount);
            session.commit();
        } catch (Exception e) {
            //回滚
            session.rollback();
            e.printStackTrace();
        } finally {
            //关闭资源
            session.close();
        }

    }


    /**
     * 查询总条数
     */
    @Test
    public void testCount() throws IOException {
        SqlSessionFactory factory =  MyBatisUtils.getFactory();

        SqlSession session = factory.openSession();

        Integer count = session.selectOne("helloworld.selectCount");
        System.out.println("count = " + count);

        session.close();
    }

    @Test
    public void testSelectList() throws IOException {

        //打开会话
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        SqlSession sqlSession = factory.openSession();

        //执行sql语句
        String statement = "helloworld.selectDept2";//指的是sql语句（MapperStatement）的ID
        List<Dept> deptList = sqlSession.selectList(statement);

        //jdk 8的用法
        deptList.forEach(System.out::println);

        sqlSession.close();

    }


    @Test
    public void testOneOrList() throws IOException {

        //打开会话
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        SqlSession sqlSession = factory.openSession();

        //执行sql语句
        String statement = "helloworld.selectDept2";
//        List<Dept> deptList = sqlSession.selectList(statement);
        Dept dept = sqlSession.selectOne(statement);

        System.out.println("dept = " + dept);

        sqlSession.close();


    }

    @Test
    public void testOneOrList2() throws IOException {

        SqlSessionFactory factory = MyBatisUtils.getFactory();
        //设计事务默认不自动提交
        SqlSession session = factory.openSession();

        Integer id = 1;
        List list = session.selectList("helloworld.selectById", id);

        System.out.println(list);

        session.close();

    }
}
