package com.neuedu.mybatis.dao;

import com.neuedu.mybatis.entity.Dept;
import com.neuedu.mybatis.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.io.IOException;
import java.util.List;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-15   9:32
 * 作者 :jshand
 * 描述 :
 */
public class DeptDao {

    
    public Dept selectById(Integer id) throws IOException {

        SqlSessionFactory factory = MyBatisUtils.getFactory();
        //设计事务默认不自动提交
        SqlSession session = factory.openSession();

        Dept dept = session.selectOne("DeptDao.selectById", id);

        session.close();

        return dept;
    }


    
    public int insert(Dept dept ) throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        //设计事务默认不自动提交
        SqlSession session = factory.openSession();
        int count = 0;
        try {
            String statementId = "DeptDao.insert";
            count = session.insert(statementId, dept);
            //session提交事务
            session.commit();
        }catch (Exception e){
            session.rollback();
            e.printStackTrace();
        }finally {
            //关闭资源
            session.close();
        }

        return count;
    }

    
    public int update( Dept dept) throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        //设计事务默认不自动提交
        SqlSession session = factory.openSession();
        int count = 0;
        try {
            //更新操作
            count = session.update("DeptDao.updateByPrimaryKey", dept);
            //提交事务
            session.commit();
        } catch (Exception e) {
            //回滚
            session.rollback();
            e.printStackTrace();
        } finally {
            //关闭资源
            session.close();
        }

        return count;
    }

    
    public int deleteByPrimaryKey( Integer id) throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        //设计事务默认不自动提交
        SqlSession session = factory.openSession();
        int deleteCount = 0;
        try {
            deleteCount = session.delete("DeptDao.deleteByPrimaryKey", id);
            session.commit();
        } catch (Exception e) {
            //回滚
            session.rollback();
            e.printStackTrace();
        } finally {
            //关闭资源
            session.close();
        }

        return deleteCount;

    }


    /**
     * 查询总条数
     */
    
    public int  count() throws IOException {
        SqlSessionFactory factory =  MyBatisUtils.getFactory();

        SqlSession session = factory.openSession();

        int count = session.selectOne("DeptDao.selectCount");

        session.close();

        return count;
    }

    
    public List<Dept> selectList() throws IOException {

        //打开会话
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        SqlSession sqlSession = factory.openSession();

        //执行sql语句
        String statement = "DeptDao.selectlist";
        List<Dept> deptList = sqlSession.selectList(statement);

        sqlSession.close();
        return deptList;

    }


    

}
