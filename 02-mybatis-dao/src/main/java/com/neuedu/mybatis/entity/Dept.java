package com.neuedu.mybatis.entity;

import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-14   10:44
 * 作者 :jshand
 * 描述 : 部门的实体
 */
public class Dept {
    private Integer id;
    private String name;
    private String address;
    private String leader;
    private Integer active;
    private Date createTime;
    private String useFul;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUseFul() {
        return useFul;
    }

    public void setUseFul(String useFul) {
        this.useFul = useFul;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", leader='" + leader + '\'' +
                ", active=" + active +
                ", createTime=" + createTime +
                ", useFul='" + useFul + '\'' +
                '}';
    }
}
