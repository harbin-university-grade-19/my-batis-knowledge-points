package com.neuedu.mybatis.dao;

import com.neuedu.mybatis.entity.Dept;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-15   9:39
 * 作者 :jshand
 * 描述 :
 */
public class DeptDaoTest {
    DeptDao deptDao = new DeptDao();

    @Test
    public void selectList() throws IOException {
        List<Dept> depts = deptDao.selectList();
        depts.forEach(System.out::println);
    }
    @Test
    public void selectById() {
    }

    @Test
    public void insert() {
    }

    @Test
    public void update() {
    }

    @Test
    public void deleteByPrimaryKey() {
    }

    @Test
    public void testCount() throws IOException {
        int count = deptDao.count();
        System.out.println("count = " + count);
    }


}