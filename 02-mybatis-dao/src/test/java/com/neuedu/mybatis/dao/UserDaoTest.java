package com.neuedu.mybatis.dao;

import com.neuedu.mybatis.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 项目：mybatis
 * 创建时间：  2022-03-15   10:36
 * 作者 :jshand
 * 描述 :
 */
public class UserDaoTest {

    @Test
    public void testInsert() throws IOException {
        SqlSessionFactory factory = MyBatisUtils.getFactory();
        //设计事务默认不自动提交
        SqlSession session = factory.openSession();
        int count = 0;
        try {
            String statementId = "UserDao.insert";
            Map map  = new HashMap();
            map.put("username","admin");
            map.put("password","123456");
            count = session.insert(statementId, map);
            //session提交事务
            session.commit();
        }catch (Exception e){
            session.rollback();
            e.printStackTrace();
        }finally {
            //关闭资源
            session.close();
        }
    }
}
